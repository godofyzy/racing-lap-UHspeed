#include "club.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

//检查圈速时间是否相同，相同则显示相同时间的车手信息
void check_time(float time)
{
    int fd = open("WD.txt", O_RDONLY);
   

    if(fd == -1)
        return ;

    info s;

    printf("时间和您相同的车手信息如下\n");
    while(read(fd, &s, sizeof(s)) == sizeof(s))
    {
        if(s.time == time)
        {
            printf("%f %s %s %s %s %s %d\n", s.time, s.car_no, s.disp, s.engine, s.racer, s.d_model, s.exist);
        }
    }

    close(fd);

}


//按照车型查找信息
void accord_car_no(void)
{
    int fd = open("WD.txt", O_RDONLY);
    char car_no[30];
    info info;
    printf("输入需要查找的车型\n");

    scanf("%s", car_no);

    while(read(fd, &info, sizeof(info)) != 0)
    {
        if(strcmp(car_no, info.car_no) == 0)
        {
            printf("%f %s %s %s %s %s %d\n", info.time, info.car_no, info.disp, info.engine, info.racer, info.d_model, info.exist);
        
        }    
    }

    close(fd);


}



//从文件中读取所有信息
void read_from_file(void)
{
    int fd;

    info info;

    fd = open("WD.txt", O_RDONLY);
   
    printf("车辆信息统计如下\n");
    printf("圈速时间\t车型\t\t排量\t      发动机\t\t车手\t\t驱动方式\t\n");
    while(read(fd, &info, sizeof(info)) != 0)
    {
        
        
        if(info.exist != 0)
        {
            printf("%.3f\t\t%s\t\t%s\t\t%s\t\t%s\t\t%s\t\t%d\t\n", info.time, info.car_no, info.disp, info.engine, info.racer, info.d_model, info.exist);
            printf("\n");
            continue;
        }
        
    }

    close(fd);

}


void read_all(void)
{
    int fd;

    info info;

    fd = open("WD.txt", O_RDONLY);
   
    printf("车辆信息统计如下\n");
    printf("圈速时间\t车型\t\t排量\t      发动机\t\t车手\t\t驱动方式\t\n");
    while(read(fd, &info, sizeof(info)) != 0)
    {
        
        
    
            printf("%.3f    %s     %s      %s      %s      %s        %d \n", info.time, info.car_no, info.disp, info.engine, info.racer, info.d_model, info.exist);
            printf("\n");
            continue;
        
        
    }

    close(fd);

}


//按照车型和车手修改信息

void replace_info(void)
{
   // umask(0000);
    int fd = open("WD.txt", O_RDWR);
    int cnt = 0;
    
    char cars[30];

    info change;

    printf("请输入相关车型\n");
        scanf("%s", cars);
      
    while(read(fd, &change, sizeof(change)) != 0)
    {
      
       
        
        if(strcmp(change.car_no, cars) == 0)
        {
            
            lseek(fd, cnt * sizeof(change), SEEK_SET);       
            printf("%d\n" , cnt);
            
               printf("输入你的圈速:单位为分/秒\n");//圈速-1>0.00 <0.60
            
                while(1)
                {
                    if(1 == scanf("%f", &change.time) ) 
                    { 
                        if(change.time > 100 || change.time < 1 || (change.time - (int)change.time) > 0.59 )
                        {
                            printf("圈速输入错误,请重新输入\n");
                            continue;
                        }
                        break;
                    }
                    else if(scanf("%f", &change.time) == 0)
                    {
                        printf("圈速输入错误,请确认清楚\n");
                        printf("请按照正确输入\n");
                        //printf("本次将退出系统\n"); 
                        //exit(1);
                        while(getchar() != '\n');
                        continue;
                    }   
               
                }
                fsync(fd);

                printf("输入你的车型:\n");
                scanf("%s", change.car_no);
                fsync(fd);

                printf("输入车辆排量:\n");
                scanf("%s", change.disp);
                fsync(fd);
                
                printf("输入车辆发动机型号:\n");
                scanf("%s", change.engine);
                fsync(fd);

                printf("输入车手名称:\n");
                scanf("%s", change.racer);  //不用合理性判断只需要判断长度
                fsync(fd);

                
                printf("输入车辆驱动方式：\n");
                scanf("%s", change.d_model);
                fsync(fd);

                change.exist = 1;  
            lseek(fd, 0, SEEK_CUR); 
            write(fd, &change, sizeof(change));

            break;
        }
        cnt ++;
        
      

    }

        printf("修改成功\n");
        close(fd);
       

}





void delete_info(void)
{
   // umask(0000);
    int fd = open("WD.txt", O_RDWR );
    int cnt = 0;
    
    char cars[30];

    info change;

    printf("请输入相关车型\n");
        scanf("%s", cars);
      
    while(read(fd, &change, sizeof(change)) != 0)
    {
      
       
        
        if(strcmp(change.car_no, cars) == 0)
        {
            
                  
            printf("%d\n" , cnt);
            change.exist = 0;  
            lseek(fd, -sizeof(change), SEEK_CUR);
            write(fd, &change, sizeof(change));

            break;
        }
        cnt ++;
        
      

    }

        printf("删除成功\n");
        close(fd);
       

}



void recover_info(void)
{
   // umask(0000);
    int fd = open("WD.txt", O_RDWR);
    int cnt = 0;
    
    
    char cars[30];
    deleted_info_in_file();
    info change;
    info* p = &change;
loop3:
    printf("请输入想要恢复相关车型信息\n");
        
        scanf("%s", cars);
        
    while(read(fd, &change, sizeof(change)) != 0)
    {
      
    
        
        if(strcmp(change.car_no, cars) == 0 )
        {
            
                 
            printf("%d\n" , cnt);
            p->exist = 1;  
            lseek(fd, -(sizeof(change)), SEEK_CUR);
            write(fd, &change, sizeof(change));

            break;
        }
        else
        {
            printf("相关信息核对有误，请重新输入\n");
            
            goto loop3;
        }
        ;
        
      

    }

        printf("恢复成功\n");
        close(fd);
       

}



void deleted_info_in_file(void)
{
    int fd;

    info info;

    fd = open("WD.txt", O_RDONLY);
   
    printf("被删除的车辆信息统计如下\n");
   printf("圈速时间\t车型\t\t排量\t      发动机\t\t车手\t\t驱动方式\t\n");
    while(read(fd, &info, sizeof(info)) != 0)
    {
        
        
        if(info.exist != 1)
        {
            printf("%.3f\t%s\t%s\t%s\t%s\t%s\t%d\t\n", info.time, info.car_no, info.disp, info.engine, info.racer, info.d_model, info.exist);
            printf("\n");
            continue;
        }
        
    }

    close(fd);

}


/*
void sort_list(info* s, int len)
{

    int i, j;

    info ex;

    for(i = 0; i < len; i++)
    {
        for(j  = 0; j < len - 1 - i; j++)
        {
            if(s[j].time > s[j + 1].time)
            {
                ex = s[j];
                s[j] = s[j + 1];
                s[j] = ex;

            }


        }

    }

}
*/









void fast_sort(info* b, int len)
{
    int i = 0, j = len - 1;
    info ex;
    ex = b[0];



    if(len < 2 )  return ;

    while(i < j)
    {
        while(i < j && b[j].time > ex.time) j--;
        b[i] = b [j];

        while(i < j && b[i].time < ex.time) i++;
        b[j] = b[i]; 

    }

    b[i] = ex;

    fast_sort(b, i);
    fast_sort(b + i + 1, len - i - 1);


}
