#ifndef __CLUB_H__
#define __CLUB_H__


typedef struct info
{
    float time;//圈速
    char car_no[30];//车型
    char disp[5];//排量
    char engine[20];//发动机型号
    char racer[50];//车手
    char d_model[10];//驱动方式
    int exist;

}info;

void check_time(float);

//int check_drive_model(char d[20]);  

void accord_car_no(void);           //根据车型查找相关数据

void read_from_file(void);          //从文件中读取未被删除的数据

void replace_info(void);            //替换修改数据

void delete_info(void);

void deleted_info_in_file(void);    //显示删除的信息以便于恢复

void recover_info(void);            //恢复已经删除的数据

void read_all(void);

void fast_sort(info* s, int len);

#endif